/* Відповіді:
    Рекурсія використовується у програмуванні для більш спрощеного та зручного написання коду, який виконує багато 
    однотипних або дуже схожих дій.
*/

function factorial(n) {
  if (n == 1) {
      return n
  }
  else if (n > 1) {
      return n * factorial(n - 1);
  }
}

let n = prompt('Number?');
while (n == "" || isNaN(n)) {
    n = prompt('Incorrect, try again.', n) 
}

alert(factorial(n)); 



